# tmux-ide
simple shell script to automate intiating a tmux coding session

## intro
while this was initially intended for my node environment, I intend to extend it for my haskell environment, too.

## use
`ide path/to/src`
